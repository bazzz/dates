package dates

// ISO8601Date is a format string for ISO8601 yyyy-MM-dd date format.
const ISO8601Date = "2006-01-02"
