# dates

Package dates provides a data type for just dates (without time) named Date which is a moment in time specified by only year month day. Also, Date does have some basic validity checks but does not check for invalid combinations like 29 February on a non leap year or so. Date is closer to how string works, rather than how time.Time works. For the real thing, you probably want to use time.Time, however Date is easy for simple things.
