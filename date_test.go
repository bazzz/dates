package dates

import (
	"encoding/json"
	"encoding/xml"
	"testing"
)

func TestString(t *testing.T) {
	date, err := New(2021, 4, 7)
	if err != nil {
		t.Fatal(err)
	}
	result := date.String()
	expected := "2021-04-07"
	if result != expected {
		t.Fatal("Result", result, "!=", expected)
	}
}

type dateHolder struct {
	Date Date `json:"date"`
}

func TestJsonUnmarshal(t *testing.T) {
	data := []byte("{\"date\":\"2021-04-07\"}")
	result := dateHolder{}
	err := json.Unmarshal(data, &result)
	if err != nil {
		t.Fatal(err)
	}
	expectedYear := 2021
	if result.Date.Year() != expectedYear {
		t.Fatal("Result Year()", result.Date.Year(), "!=", expectedYear)
	}
	expectedMonth := 4
	if result.Date.Month() != expectedMonth {
		t.Fatal("Result Month()", result.Date.Month(), "!=", expectedMonth)
	}
	expectedDay := 7
	if result.Date.Day() != expectedDay {
		t.Fatal("Result Day()", result.Date.Day(), "!=", expectedDay)
	}
}

func TestJsonUnmarshalEmpty(t *testing.T) {
	data := []byte("{\"date\":\"\"}")
	result := dateHolder{}
	err := json.Unmarshal(data, &result)
	if err != nil {
		t.Fatal(err)
	}
	expectedYear := 0
	if result.Date.Year() != expectedYear {
		t.Fatal("Result Year()", result.Date.Year(), "!=", expectedYear)
	}
	expectedMonth := 0
	if result.Date.Month() != expectedMonth {
		t.Fatal("Result Month()", result.Date.Month(), "!=", expectedMonth)
	}
	expectedDay := 0
	if result.Date.Day() != expectedDay {
		t.Fatal("Result Day()", result.Date.Day(), "!=", expectedDay)
	}
}

func TestParse(t *testing.T) {
	result, err := Parse(ISO8601Date, "2021-04-07")
	if err != nil {
		t.Fatal(err)
	}
	expectedYear := 2021
	if result.Year() != expectedYear {
		t.Fatal("Result Year()", result.Year(), "!=", expectedYear)
	}
	expectedMonth := 4
	if result.Month() != expectedMonth {
		t.Fatal("Result Month()", result.Month(), "!=", expectedMonth)
	}
	expectedDay := 7
	if result.Day() != expectedDay {
		t.Fatal("Result Day()", result.Day(), "!=", expectedDay)
	}
}

func TestXMLMarshal(t *testing.T) {
	date := Date{
		year:  2021,
		month: 4,
		day:   7,
	}
	data, err := xml.Marshal(&date)
	if err != nil {
		t.Fatal(err)
	}
	expected := "<Date>2021-04-07</Date>"
	if string(data) != expected {
		t.Fatal("Result ", string(data), "!=", expected)
	}
}

func TestXMLMarshalWithIndent(t *testing.T) {
	type Holder struct {
		XMLName   xml.Name `xml:"holder"`
		Name      string   `xml:"name"`
		Premiered Date     `xml:"premiered"`
	}
	holder := Holder{
		Name: "Test",
		Premiered: Date{
			year:  2021,
			month: 4,
			day:   7,
		},
	}
	data, err := xml.MarshalIndent(&holder, "", "\t")
	if err != nil {
		t.Fatal(err)
	}
	expected := "<holder>\n\t<name>Test</name>\n\t<date>2021-04-07</date>\n</holder>"
	if string(data) != expected {
		t.Fatal("Result ", string(data), "!=", expected)
	}
}
