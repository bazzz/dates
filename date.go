package dates

import (
	"errors"
	"strconv"
	"strings"
)

// Date represents a moment in time specified by year, month and day.
type Date struct {
	year  int
	month int
	day   int
}

// Year returns the year part of date.
func (d Date) Year() int {
	return d.year
}

// Month returns the month part of date.
func (d Date) Month() int {
	return d.month
}

// Day returns the day part of date.
func (d Date) Day() int {
	return d.day
}

// String returns the date in ISO8601 yyyy-MM-dd format.
func (d Date) String() string {
	year := strconv.Itoa(d.year)
	for len(year) < 4 {
		year = "0" + year
	}
	month := strconv.Itoa(d.month)
	if len(month) == 1 {
		month = "0" + month
	}
	day := strconv.Itoa(d.day)
	if len(day) == 1 {
		day = "0" + day
	}
	return year + "-" + month + "-" + day
}

// New constructs a new Date from the specified values.
func New(year int, month int, day int) (Date, error) {
	if year < 1 {
		return Date{}, errors.New("year must be more than 0")
	}
	if month < 1 || month > 12 {
		return Date{}, errors.New("month must be between 1 and 12 inclusive")
	}
	if day < 1 || day > 31 {
		return Date{}, errors.New("day must be between 1 and 31 inclusive")
	}
	if (month == 4 || month == 6 || month == 9 || month == 11) && day > 30 {
		return Date{}, errors.New("month" + strconv.Itoa(month) + " does not allow for day to be more than 30")
	}
	if month == 2 && day > 29 {
		return Date{}, errors.New("month 2 does not allow for day to be more than 29")
	}
	return Date{year: year, month: month, day: day}, nil
}

// Parse constructs a new date from the input string accepting format similar to time.Parse, but limited to: 2006 for year, 01 for month, and 02 for day.
func Parse(format string, input string) (Date, error) {
	yearformat := "2006"
	monthformat := "01"
	dayformat := "02"
	if len(format) != len(input) {
		return Date{}, errors.New("format and input must be of equal length")
	}
	yearpos := strings.Index(format, yearformat)
	if yearpos < 0 {
		return Date{}, errors.New("format does not include " + yearformat + " as indication for year")
	}
	year, err := strconv.Atoi(input[yearpos : yearpos+len(yearformat)])
	if err != nil {
		return Date{}, errors.New("input year could not be parsed as an integer")
	}
	monthpos := strings.Index(format, monthformat)
	if monthpos < 0 {
		return Date{}, errors.New("format does not include " + monthformat + " as indication for month")
	}
	month, err := strconv.Atoi(input[monthpos : monthpos+len(monthformat)])
	if err != nil {
		return Date{}, errors.New("input month could not be parsed as an integer")
	}
	daypos := strings.Index(format, dayformat)
	if daypos < 0 {
		return Date{}, errors.New("format does not include " + dayformat + " as indication for day")
	}
	day, err := strconv.Atoi(input[daypos : daypos+len(dayformat)])
	if err != nil {
		return Date{}, errors.New("input day could not be parsed as an integer")
	}
	return New(year, month, day)
}

// MarshalText allows Date to marshal itself into a textual form, for example to JSON or XML.
func (d *Date) MarshalText() ([]byte, error) {
	return []byte(d.String()), nil
}

// UnmarshalText allows Date to unmarshal a textual representation of itself, for example from JSON or XML.
func (c *Date) UnmarshalText(data []byte) error {
	s := string(data)
	s = strings.TrimPrefix(s, "\"")
	s = strings.TrimSuffix(s, "\"")
	if s == "" {
		c = nil
		return nil
	}
	result, err := Parse(ISO8601Date, s)
	if err != nil {
		return err
	}
	c.year = result.year
	c.month = result.month
	c.day = result.day
	return nil
}
